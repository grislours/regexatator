<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/regexatator/mainwindow.ui" line="14"/>
        <source>Regexatator</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/regexatator/mainwindow.ui" line="25"/>
        <source>Regular expression:</source>
        <translation>Expression régulière :</translation>
    </message>
    <message>
        <location filename="../src/regexatator/mainwindow.ui" line="55"/>
        <source>std::regex</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/regexatator/mainwindow.ui" line="65"/>
        <source>boost::regex</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/regexatator/mainwindow.ui" line="72"/>
        <source>QRegularExpression</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/regexatator/mainwindow.ui" line="95"/>
        <source>Copy</source>
        <translation>Copie</translation>
    </message>
    <message>
        <location filename="../src/regexatator/mainwindow.ui" line="106"/>
        <source>Text:</source>
        <translation>Texte :</translation>
    </message>
    <message>
        <location filename="../src/regexatator/mainwindow.ui" line="116"/>
        <source>Result:</source>
        <translation>Résultat :</translation>
    </message>
    <message>
        <location filename="../src/regexatator/mainwindow.cpp" line="44"/>
        <source>(not matching)</source>
        <translation>(pas de correspondance)</translation>
    </message>
    <message>
        <location filename="../src/regexatator/mainwindow.cpp" line="60"/>
        <source>(none)</source>
        <translation>(absent)</translation>
    </message>
    <message>
        <location filename="../src/regexatator/mainwindow.cpp" line="78"/>
        <source>Error: </source>
        <translation>Erreur : </translation>
    </message>
</context>
</TS>
