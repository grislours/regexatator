Regexatator - Test Your Regular Expression
==========================================

Regexatator is a graphical tool designed to help you construct
and test your regular expression.

How to Get Help
---------------

Report bugs in Regexatator by opening an issue in the
[issue tracker](https://framagit.org/grislours/Regexatator/issues).
Remember to search for existing issues before creating a new one.

Build Environment
-----------------

* C++17 compiler
  * Linux - GCC >= 8.2
* CMake >= 3.13
* Ninja (optional)

Dependencies
------------

External dependencies can be satisfied by system libraries or installed
separately.

**External Dependencies**

* Qt (required >= 5.0)
* boost (required >= 1.18)

How to Build
------------

Linux:

    git clone https://framagit.org/grislours/regexatator.git
    mkdir -p build/regexatator_release
    cd build/regexatator_release
    cmake -G Ninja -DCMAKE_BUILD_TYPE=Release ../../regexatator 

If you have Qt installed in a non-standard location, you may have to
specify the path to Qt by passing `-DCMAKE_PREFIX_PATH=<path-to-qt>`
where `<path-to-qt>` points to the Qt install directory that contains
`bin`, `lib`, etc.

**Build**

    ninja

How to Contribute
-----------------

We welcome contributions of all kinds, including bug fixes, new features,
documentation and translations. By contributing, you agree to release
your contributions under the terms of the license.

Contribute by following the typical
[GitLab workflow](https://about.gitlab.com/2014/09/29/gitlab-flow)
for pull requests. Fork the repository and make changes on a new named
branch. Create pull requests against the `master` branch. Follow the
[seven guidelines](https://chris.beams.io/posts/git-commit/) to writing a
great commit message.

License
-------

Regexatator code is licensed as AGPLv3. See LICENSE.md for details.
