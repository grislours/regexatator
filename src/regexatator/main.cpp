
#include "mainwindow.hpp"
#include "utils/qt/translator.hpp"
#include <QApplication>
#include <QLibraryInfo>

auto main( int argc, char **argv ) -> int {
  // global settings
  QCoreApplication::setOrganizationName( "gamasoft" );
  QCoreApplication::setOrganizationDomain( "gamasoft.fr" );
  QCoreApplication::setApplicationName( "regexatator" );

  // start application
  auto app = QApplication{ argc, argv };
  auto qt_translator = qt::Translator{ app, "qt_", QLibraryInfo::location( QLibraryInfo::TranslationsPath ) };
  auto app_translator = qt::Translator{ app, "regexatator_" };
  auto mainWin = MainWindow{};
  mainWin.show();
  return QApplication::exec();
}
