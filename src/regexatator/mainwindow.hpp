#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QMainWindow>
#include <QRadioButton>
#include <memory>

namespace Ui {
class MainWindow;
}

class MainWindow
  : public QMainWindow
{
  Q_OBJECT

public:
  struct PosAndLength {
    int pos = 0;
    int length = 0;

    PosAndLength( int p, int l ) : pos{ p }, length{ l } {}
  };

public:
  explicit MainWindow( QWidget *parent = nullptr );
  ~MainWindow() override;

public slots:
  void compute();
  void copy();
  void toggled( bool toggled );

protected:
  void closeEvent( QCloseEvent *event ) override;

private:
  void restore_settings();
  static auto result_stdregex( const std::string &regex_text, const std::string &text ) -> std::vector<PosAndLength>;
  static auto result_boostregex( const std::string &regex_text, const std::string &text ) -> std::vector<PosAndLength>;
  static auto result_qregularexpression( const std::string &regex_text, const std::string &text ) -> std::vector<PosAndLength>;

private:
  std::unique_ptr<Ui::MainWindow> ui_ptr_{};
  std::vector<std::reference_wrapper<QRadioButton>> radio_buttons_{};
};

#endif
