
#include "mainwindow.hpp"
#include "ui_mainwindow.h"
#include "utils/exception.hpp"
#include <QClipboard>
#include <QRegularExpression>
#include <QSettings>
#include <boost/regex.hpp>
#include <regex>

MainWindow::MainWindow( QWidget *parent )
  : QMainWindow{ parent }
  , ui_ptr_{ new Ui::MainWindow }
{
  ui_ptr_->setupUi( this );
  radio_buttons_ = {
    *ui_ptr_->radioButton_stdregex,
    *ui_ptr_->radioButton_boostregex,
    *ui_ptr_->radioButton_qregularexpression
  };
  restore_settings();
}

MainWindow::~MainWindow() = default;

void MainWindow::compute()
{
  auto result = QString{};
  try {
    // get text and regex
    const auto text = ui_ptr_->lineEdit_text->text().toStdString();
    const auto regex_text = ui_ptr_->textEdit_regex->toPlainText().remove( '\n' ).toStdString();

    // get match info
    auto matches = std::vector<PosAndLength>{};
    if( ui_ptr_->radioButton_stdregex->isChecked() ) {
      matches = result_stdregex( regex_text, text );
    } else if( ui_ptr_->radioButton_boostregex->isChecked() ) {
      matches = result_boostregex( regex_text, text );
    } else if( ui_ptr_->radioButton_qregularexpression->isChecked() ) {
      matches = result_qregularexpression( regex_text, text );
    }
    if( matches.empty() ) {
      result = tr( "(not matching)" );
      ui_ptr_->plainTextEdit_result->setPlainText( result );
    } else {
      // TODO reorganiser la partie formattage

      // display matches
      result = QString{ ui_ptr_->lineEdit_text->text() } + '\n';
      auto os = std::ostringstream{};
      auto i = 0U;
      for( auto [ pos, length ] : matches ) {
        if( i == 0 ) {
          ++i;
          continue;
        }
        os << i << ": ";
        if( pos == -1 or length == 0 ) {
          os << tr( "(none)" ).toStdString() << '\n';
        } else {
          os << text.substr( static_cast<unsigned>( pos ), static_cast<unsigned>( length ) ) << '\n';
        }
        ++i;
      }
      result += QString::fromStdString( os.str() );
      ui_ptr_->plainTextEdit_result->setPlainText( result );

      // underline match
      auto format = QTextCharFormat{};
      format.setUnderlineStyle( QTextCharFormat::SingleUnderline );
      auto cursor = ui_ptr_->plainTextEdit_result->textCursor();
      cursor.setPosition( matches.at( 0 ).pos );
      cursor.setPosition( matches.at( 0 ).pos + matches.at( 0 ).length, QTextCursor::KeepAnchor );
      cursor.mergeCharFormat( format );
    }
  } catch( const std::exception &e ) {
    result = tr( "Error: " ) + e.what();
    ui_ptr_->plainTextEdit_result->setPlainText( result );
  }
}

void MainWindow::copy()
{
  const auto regex_text = ui_ptr_->textEdit_regex->toPlainText();
  const auto lines = regex_text.split( '\n', Qt::KeepEmptyParts );
  auto result = std::string{};
  for( const auto &line : lines ) {
    result += "R\"(" + line.toStdString() + ")\"\n";
  }
  auto &clipboard = *QApplication::clipboard();
  clipboard.setText( QString::fromStdString( result ) );
}

void MainWindow::toggled( bool toggled )
{
  if( toggled ) {
    compute();
  }
}

void MainWindow::closeEvent( QCloseEvent *event )
{
  auto settings = QSettings{};
  settings.beginGroup( "main_window" );
  settings.setValue( "geometry", saveGeometry() );
  settings.setValue( "windowState", saveState() );
  settings.setValue( "regex", ui_ptr_->textEdit_regex->toPlainText() );
  settings.setValue( "text", ui_ptr_->lineEdit_text->text() );
  for( auto i = 0U; i < radio_buttons_.size(); ++i ) {
    if( radio_buttons_.at( i ).get().isChecked() ) {
      settings.setValue( "engine", i );
    }
  }
  settings.endGroup();
  QMainWindow::closeEvent( event );
}

void MainWindow::restore_settings()
{
  auto settings = QSettings{};
  settings.beginGroup( "main_window" );
  restoreGeometry( settings.value( "geometry" ).toByteArray() );
  restoreState( settings.value( "windowState" ).toByteArray() );
  ui_ptr_->textEdit_regex->setText( settings.value( "regex" ).toString() );
  ui_ptr_->lineEdit_text->setText( settings.value( "text" ).toString() );
  radio_buttons_.at( settings.value( "engine", 0U ).toUInt() ).get().setChecked( true );
  settings.endGroup();
}

auto MainWindow::result_stdregex( const std::string &regex_text, const std::string &text )
-> std::vector<PosAndLength>
{
  // test match
  const auto regex = std::regex{ regex_text };
  auto match = std::smatch{};
  const auto is_matching = std::regex_search( text, match, regex );
  if( not is_matching ) {
    return {};
  }

  // construct result
  auto matches = std::vector<PosAndLength>{};
  for( auto i = 0U; i < match.size(); ++i ) {
    matches.emplace_back( match.position( i ), match.length( i ) );
  }
  return matches;
}

auto MainWindow::result_boostregex( const std::string &regex_text, const std::string &text )
-> std::vector<PosAndLength>
{
  // test match
  const auto regex = boost::regex{ regex_text };
  auto match = boost::smatch{};
  const auto is_matching = boost::regex_search( text, match, regex );
  if( not is_matching ) {
    return {};
  }

  // construct result
  auto matches = std::vector<PosAndLength>{};
  for( auto i = 0U; i < match.size(); ++i ) {
    matches.emplace_back( match.position( i ), match.length( static_cast<int>( i ) ) );
  }
  return matches;
}

auto MainWindow::result_qregularexpression( const std::string &regex_text, const std::string &text )
-> std::vector<PosAndLength>
{
  // test match
  const auto regex = QRegularExpression{ QString::fromStdString( regex_text ) };
  auto match = regex.match( QString::fromStdString( text ) );
  if( not match.hasMatch() ) {
    return {};
  }

  // construct result
  auto matches = std::vector<PosAndLength>{};
  for( auto i = 0; i <= match.lastCapturedIndex(); ++i ) {
    matches.emplace_back( match.capturedStart( i ), match.capturedLength( i ) );
  }
  return matches;
}
