
project( regexatator )

set( CMAKE_AUTOMOC ON )
set( CMAKE_AUTOUIC ON )
set( CMAKE_AUTORCC ON )
set( CMAKE_INCLUDE_CURRENT_DIR ON )

find_package( Boost REQUIRED program_options regex )
find_package( Qt5Widgets REQUIRED )
find_package( Qt5LinguistTools REQUIRED )

# source files
set( ${PROJECT_NAME}_SOURCES
  main.cpp
  mainwindow.cpp
  mainwindow.ui
  main.qrc
)

qt5_create_translation( QM_FILES ${${PROJECT_NAME}_SOURCES} ${${PROJECT_NAME}_TS} )

# executable
include_directories( ${Boost_INCLUDE_DIRS} ${CMAKE_SOURCE_DIR}/src )
add_executable( ${PROJECT_NAME} ${${PROJECT_NAME}_SOURCES} ${QM_FILES} )
target_link_libraries( ${PROJECT_NAME} ${Boost_LIBRARIES} Qt5::Widgets )

# install
install( TARGETS ${PROJECT_NAME} RUNTIME DESTINATION bin)
