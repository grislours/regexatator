
#ifndef QT_TRANSLATOR_HPP
#define QT_TRANSLATOR_HPP

#include <QApplication>
#include <QTranslator>

namespace qt {

class Translator {
public:
  Translator(
      QApplication &app,
      const QString &prefix,
      const QString &directory = QString{}
  );

private:
  QTranslator translator_;
};

}

qt::Translator::Translator( QApplication &app, const QString &prefix, const QString &directory )
  : translator_{}
{
  translator_.load( prefix + QLocale::system().name(), directory );
  app.installTranslator( &translator_ );
}

#endif
